from sys import stdout, stdin, stderr
import subprocess, random, datetime

def vaildNegativeDecimal(valid):
    fun1 = ['n', 'd']
    fun2 = []
    i = 0
    if valid:
        for x in range(len(fun1)):
            if valid[i] == fun1[x]:
                fun2.append(valid[i])
                if i < len(valid) and (i+1) != len(valid):
                        i += 1
            else :
                fun2.append('$')
    else:
        for x in range(len(fun1)):
            fun2.append('$')
    return fun2

def generateOperationArithmetic(sizeOperation, arrayValidNegativeDecimal, typeOperation):
    p = subprocess.Popen([f'java -jar codes/generators/arithmetic/code/arithmetic.jar {sizeOperation} {arrayValidNegativeDecimal[0]} {arrayValidNegativeDecimal[1]} {typeOperation}'], stdout=subprocess.PIPE, stdin=subprocess.PIPE, shell = True)
    s = p.communicate()[0].decode('utf-8')
    s = s.replace(" ", ",")
    s = s.split(',')
    f = s[0] + " " + s[1] + " " + s[2] + " " + s[3]
    result = [f, s[4]]
    return result

def generateOperationInequality(sizeOperation):
    p = subprocess.Popen ([f'./codes/generators/inequality/code/generateInequality {sizeOperation}'], stdout=subprocess.PIPE, stdin=subprocess.PIPE, shell = True)
    s = p.communicate()[0].decode('utf-8')
    s = s.replace(" ", ",")
    s = s.split(',')
    f = s[0] + " " + s[1] + " " + s[2]
    result = [f, s[3]]
    return result

def generationCodeClass(idUser):
    L=list('abc1def2gh3ij4kl5mn6opq7r8stu9vw0xyz')
    f=[]
    cl =" "
    for i in range(1):
        p=''
        for j in range(10):
            p+=L[random.randint(0,25)]
        f.append(''.join(p))
    result = cl.join(f) + str(idUser)
    return result

def operationSelect(operation):
    if operation == 'aleatory':
        r = int(random.uniform(1, 5))
        if r == 1:
            operative = '+'
        if r == 2:
            operative = '-'
        if r == 3:
            operative = 'z'
        if r == 4:
            operative = '/'
    else:
        operative = operation
    return operative

def sumTime(now, duration):
    duration = duration.split(':')
    now = datetime.datetime.strptime(now, "%X" )
    d = datetime.timedelta(hours=int(duration[0]), minutes=int(duration[1]))
    sum = now + d
    return sum

def validationProblem(answer, result):
    p = subprocess.Popen ([f'java -jar codes/validation/simple/validation.jar {answer} {result}'], stdout=subprocess.PIPE, shell = True)
    s = p.communicate()[0].decode('utf-8')
    return s

def convertList(dateToList):
    resutlt = dateToList.split(':')
    return resutlt

