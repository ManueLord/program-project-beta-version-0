from django.apps import AppConfig


class CenterConfig(AppConfig):
    name = 'center'

    def ready(self):
        import center.signals