import  time, datetime
from django.utils import timezone
from django.shortcuts import render, redirect
from django.http import HttpResponse
from django.contrib import messages
from django.contrib.auth import update_session_auth_hash
from django.contrib.auth.models import Group, User
from django.contrib.auth.decorators import login_required
from .funtions import *
from .forms import *
from .models import *

# Create your views here.
def index(request):
    return render (request, "app/index.html")

def RegisterUser(request):
    if request.method == 'POST':
        form = RegisterForm(request.POST)
        if form.is_valid():
            user = form.save()
            group = Group.objects.get(name='student')
            user.groups.add(group)
            username = form.cleaned_data.get('username')
            messages.success(request, f'You account has been created for {username}!. You are now able to log in')
            return redirect('login')
    else:
        form = RegisterForm()
    return render(request, 'users/register.html', {'form': form})

@login_required
def profile(request):
    if request.method == 'POST':
        u_form = UserUpdateFrom(request.POST, instance=request.user)
        p_form = ProfileUpdateForm(request.POST, request.FILES, instance=request.user.profile)
        if u_form.is_valid() and p_form.is_valid:
            u_form.save()
            p_form.save()
            username = request.user.username
            messages.success(request, f'You account has been updateiqeakrl3gb3d for {username}!.')
            return redirect('profile')
    else:
        u_form = UserUpdateFrom(instance=request.user)
        p_form = ProfileUpdateForm(instance=request.user.profile)
    context = {'u_form': u_form, 'p_form': p_form}
    
    return render(request, "users/profile.html", context)
    
@login_required
def changePass(request):
    if request.method == 'POST':
        form = PasswordUpdateFrom(request.user, request.POST)
        if form.is_valid():
            user = form.save()
            update_session_auth_hash(request, user)  # Important!
            messages.success(request, 'Your password was successfully updated!')
            return redirect('pass')
        else:
            messages.error(request, 'Please correct the error below.')
    else:
        form = PasswordUpdateFrom(request.user)
    return render(request, "users/password.html", {'form': form})

@login_required
def listPractices(request):
    liste = OperationsPractics.objects.filter(users_id=request.user.id)
    cou = 13 - OperationsPractics.objects.filter(users_id=request.user.id).count()
    context = {"list": liste, "range": range(cou)}
    return render(request, 'app/listPractices.html', context)

@login_required
def createPractice(request):
    query = TypeClass.objects.all()
    context = {"query": query}
    return render (request, "app/createPractice.html", context)

@login_required
def generationPractice(request):
    if request.method == 'POST':
        r1 = int(request.POST.get("tip"))
        r2 = int(request.POST.get("size"))
        r3 = str(request.POST.get("operation"))
        e = request.POST.getlist("extras")
        con = OperationsPractics.objects.filter(users_id = request.user.id).all().count()
        title = "Operation " + str(con+1)
        nd = vaildNegativeDecimal(e)
        if r1 == 1:
            goa = generateOperationArithmetic(r2, nd, r3)
            OperationsPractics.objects.create(title=title, formula=goa[0], results=goa[1], type_c_id=r1, users_id=request.user.id)
        if r1 == 2:
            goi = generateOperationInequality(r2)
            OperationsPractics.objects.create(title=title, formula=goi[0], results=goi[1], type_c_id=r1, users_id=request.user.id)
        l = OperationsPractics.objects.filter(users_id=request.user.id).order_by('-id')[0]
        la = str(l)
        las = list(la.split(" "))
        last = las[2].replace("(", "").replace(")", "")
    return redirect('practice/%d' %int(last))

@login_required
def practice(request, pk):
    problem = OperationsPractics.objects.get(id = pk)
    typ = OperationsPractics.objects.select_related('type_c').all().filter(id = pk)
    context = {'p': problem, "type": typ}
    return render (request, "app/practice.html", context)

@login_required
def validationPractice(request, pk):
    if request.method=='POST':
        res = ""
        if'btn1' in request.POST:
            res = "mayor_que"
        if'btn2' in request.POST:
            res = "menor_que"
        if'btn3' in request.POST:
            res = "igual"
        if'btn4' in request.POST:
            res = request.POST.get('res')
        r = OperationsPractics.objects.get(id = pk)
        vp = validationProblem(res, r.results)
        OperationsPractics.objects.filter(id=pk).update(validation=vp, your_results=res)
    return redirect('listPractices')

@login_required
def createClass(request):
    query = TypeClass.objects.all()
    context= {'query': query}
    if request.method == 'POST':
        r1 = request.POST.get('nameClass')
        r3 = int(request.POST.get('tip'))
        gcc = generationCodeClass(request.user.id)
        typeC = TypeClass.objects.get(id=r3)
        tea = User.objects.get(id=request.user.id)
        #Create: es el metodo para guardar en django
        Classes.objects.create(name=r1, codes=gcc, teacher=tea, type_c=typeC)
        return redirect('listCalassrooms')

    return render(request, "rooms/createClass.html", context)

@login_required
def listCalassrooms(request):
    #Valid code of class
    a = " "
    if request.method == 'POST':
        r = str(request.POST.get('classIns'))

        if Classes.objects.filter(codes=r):
            Clas = Classes.objects.get(codes=r)
            te = r[10:len(r)]

            tea = User.objects.get(id=te)
            stu= User.objects.get(username=request.user.username)
            Classrooms.objects.create(student=stu, teacher=tea, classess=Clas)
            messages.success(request, "It was found... ;)")
            return redirect('listCalassrooms')
        else:
            messages.warning(request, "It was not found... !")
            return redirect('listCalassrooms')
    query_set = Group.objects.filter(user = request.user)

    #List classrooms
    for g in query_set:
        a = g.name
    if a == "student":
        #El select_related: es un inner join del metodo de django y es a fuerzas usar los ForeignKey
        query = Classrooms.objects.select_related('classess').all().filter(student=request.user.id)
    if a == "teacher":
        query = Classes.objects.filter(teacher=request.user.id)
    context= {'query': query}
    return render(request, "rooms/listClassrooms.html", context)

@login_required
def classroom(request, name):
    n = Classes.objects.filter(name=name)
    for fil in n:
        te = fil.codes
        te = te[10:len(te)]
    n = Classes.objects.get(name=name, teacher_id= te)
    c = Comments.objects.filter(classess_id = n.id)
    if request.method == 'POST':
        m = request.POST.get('msj')
        if m:
            Comments.objects.create(user_comment_id= request.user.id, text=m, classess_id=n.id)
        return redirect('/listCalassrooms/%s' %name)
    context = {'n':name, 'com': c}
    return render(request, "rooms/classroom.html", context)

@login_required
def members(request, name):
    te = Classes.objects.filter(name=name)
    for fil in te:
        f = fil.codes
        f = f[10:len(f)]
    te = Classes.objects.get(name=name, teacher_id= f)
    tea = str(te.codes)
    teach = tea[10:len(tea)]
    teacher = Classes.objects.select_related('teacher').all().filter(name = name, teacher_id= f)
    student = Classrooms.objects.select_related('student').all().filter(classess_id=te)
    cou = 12 - Classrooms.objects.filter(classess_id=te.id).count()
    context = {"tea": teacher, "n": name, "stu":student, "range": range(cou)}
    return render(request, "rooms/members.html", context)

@login_required
def listHomework(request, name):
    c = Classes.objects.filter(name=name)
    for fil in c:
        f = fil.codes
        f = f[10:len(f)]
    c = Classes.objects.get(name=name, teacher_id= f)
    listH = WorkActivity.objects.filter(type_a_id=1, classess_id=c.id)
    context = {"n": name, 'list': listH}
    return render(request, "rooms/listHomework.html", context)

@login_required
def listExam(request, name):
    c = Classes.objects.filter(name=name)
    for fil in c:
        f = fil.codes
        f = f[10:len(f)]
    c = Classes.objects.get(name=name, teacher_id= f)
    listE = WorkActivity.objects.filter(type_a_id=2, classess_id=c.id)
    context = {"n": name, 'list': listE}
    return render(request, "rooms/listExam.html", context)

@login_required
def createHomework(request,name):
    c = Classes.objects.filter(name=name)
    for fil in c:
        f = fil.codes
        f = f[10:len(f)]
    c = Classes.objects.get(name=name, teacher_id= f)
    q = TypeActivity.objects.all()
    d = timezone.now().strftime("%Y-%m-%d")
    context = {"d":d, "query": q, "type":c.type_c_id, "name": name}
    return render(request, "rooms/createHomework.html", context)

@login_required
def createExam(request,name):
    c = Classes.objects.filter(name=name)
    for fil in c:
        f = fil.codes
        f = f[10:len(f)]
    c = Classes.objects.get(name=name, teacher_id= f)
    q = TypeActivity.objects.all()
    d = timezone.now().strftime("%Y-%m-%d")
    context = {"d":d, "query": q, "type":c.type_c_id, "name": name}
    return render(request, "rooms/createExam.html", context)

def generationWork(request,name):
    if request.method == 'POST':
        e = int(request.POST.get('work'))
        c = Classes.objects.filter(name=name)
        for fil in c:
            f = fil.codes
            f = f[10:len(f)]
        c = Classes.objects.get(name=name, teacher_id= f)
        f1 = ['n', 'd']
        if request.method == 'POST':
            n = request.POST.get('name')
            e = int(request.POST.get('work'))
            q = request.POST.get('quantity')
            s = int(request.POST.get("size"))
            o = request.POST.get('operation')
            ex = request.POST.getlist('extras')
            fh = request.POST.get('bday')
            vnd = vaildNegativeDecimal(ex)
            if e == 2:
                h = request.POST.get('hour')
                m = request.POST.get('minu')
                d = h + ":" + m + ":00"
            else:
                d = ""
            students = Classrooms.objects.filter(classess_id=c.id)
            WorkActivity.objects.create(name_work=n, finish_date=fh, type_a_id=e, duration = d, classess_id=c.id)
            wa = WorkActivity.objects.get(name_work=n, classess_id=c.id)
            for stu in students:
                for x in range (int(q)):
                    t = str(x+1)
                    if c.type_c_id == 1:
                        op =operationSelect(o)
                        operation = generateOperationArithmetic(s, vnd, op)
                    if c.type_c_id == 2:
                        operation = generateOperationInequality(s)
                    OperationsWorkActivity.objects.create(title=t, formula=operation[0], results=operation[1], users_id=stu.student_id, work_id=wa.id)
                    time.sleep(1)
                AveragesWorkActivity.objects.create(average=0, student_id=stu.student_id, type_a_id=wa.id)
    if e == 1:
        #HomeWork
        t = "Create New Homework"
        Comments.objects.create(user_comment_id=request.user.id, text=t, classess_id=c.id)
        return redirect("/listCalassrooms/%s/listHomework" %name)
                        
    else:
        #Exam
        t = "Create New Exam"
        Comments.objects.create(user_comment_id=request.user.id, text= t, classess_id=c.id)
        return redirect("/listCalassrooms/%s/listExam" %name)

def work(request, name, name_work):
    c = Classes.objects.filter(name=name)
    for fil in c:
        f = fil.codes
        f = f[10:len(f)]
    c = Classes.objects.get(name=name, teacher_id= f)
    wa = WorkActivity.objects.get(name_work = name_work, classess_id=c.id)
    awa = AveragesWorkActivity.objects.filter(type_a_id=wa.id)
    query_set = Group.objects.filter(user = request.user)
    for g in query_set:
        a = g.name
    if a == "student":
        away = AveragesWorkActivity.objects.get(type_a_id=wa.id, student_id=request.user.id)
    cou = 13 - awa.count()
    if request.method == 'POST':
        if wa.type_a_id == 1:
            return redirect(f"/listCalassrooms/{name}/listHomework/{name_work}/homework/")
        if wa.type_a_id == 2:
            if away.active == False:
                AveragesWorkActivity.objects.filter(student_id=request.user.id, type_a_id=wa.id).update(active=True)
                return redirect(f"/listCalassrooms/{name}/listExam/{name_work}/exam/")
            if away.active == True:
                return redirect(f"/listCalassrooms/{name}/listExam/{name_work}/")
    if a == "student":
        context = {"valid": wa, "awa": awa, 'range': range(cou), "s": away.average, "name": name}
    else:
        context = {"valid": wa, "awa": awa, 'range': range(cou), "name": name, "name_work": name_work}
    return render(request, "rooms/work.html", context)

def homework(request, name, name_work):
    c = Classes.objects.filter(name=name)
    for fil in c:
        f = fil.codes
        f = f[10:len(f)]
    c = Classes.objects.get(name=name, teacher_id= f)
    wa = WorkActivity.objects.get(name_work = name_work, classess_id=c.id)
    owa = OperationsWorkActivity.objects.filter(work_id=wa.id, users_id=request.user.id)
    awa = AveragesWorkActivity.objects.get(type_a_id=wa.id, student_id=request.user.id)
    owa_c = owa.count()
    if owa_c <= 3:
        ran = 10
    if owa_c <= 6 and owa_c > 3:
        ran = 8
    if owa_c <= 9 and owa_c > 6:
        ran = 6
    if owa_c <= 12 and owa_c > 9:
        ran = 4
    if owa_c <= 15 and owa_c > 12:
        ran = 2
    if request.method == 'POST':
        if'btnA':
            for x in range(1, owa_c + 1):
                owa_s = OperationsWorkActivity.objects.get(title=x, work_id=wa.id, users_id=request.user.id)
                inp = request.POST.get(f"r{x}")
                d = inp
                if owa_s.your_results == None:
                    if inp != '':
                        vp = validationProblem(d, owa_s.results)
                        OperationsWorkActivity.objects.filter(work_id=wa.id, users_id=request.user.id, title=str(x)).update(validation=vp, your_results=d)
                        if vp == "Correct Answer":
                            owas = OperationsWorkActivity.objects.filter(work_id=wa.id, users_id=request.user.id, validation="Correct Answer").count()
                            pro = (owas/owa_c)*100
                            AveragesWorkActivity.objects.filter(type_a_id=wa.id, student_id=request.user.id).update(average=pro)
    finish = all(x.validation != None for x in owa)
    context = {"owa":owa, "name": name, "name_work": name_work, "range": range(ran),"finish": finish, "wa": wa}
    return render(request, "rooms/homework.html", context)

def exam(request, name, name_work):
    c = Classes.objects.filter(name=name)
    for fil in c:
        f = fil.codes
        f = f[10:len(f)]
    c = Classes.objects.get(name=name, teacher_id= f)
    wa = WorkActivity.objects.get(name_work = name_work, classess_id=c.id)
    owa = OperationsWorkActivity.objects.filter(work_id=wa.id, users_id=request.user.id)
    awa = AveragesWorkActivity.objects.get(type_a_id=wa.id, student_id=request.user.id)
    owa_c = owa.count()
    if owa_c <= 3:
        ran = 10
    if owa_c <= 6 and owa_c > 3:
        ran = 8
    if owa_c <= 9 and owa_c > 6:
        ran = 6
    if owa_c <= 12 and owa_c > 9:
        ran = 4
    if owa_c <= 15 and owa_c > 12:
        ran = 2
    if request.method == 'POST':
        if'btnA':
            for x in range(1, owa_c + 1):
                owa_s = OperationsWorkActivity.objects.get(title=x, work_id=wa.id, users_id=request.user.id)
                inp = request.POST.get(f"r{x}")
                d = inp
                if owa_s.your_results == None:
                    if inp != '':
                        vp = validationProblem(d, owa_s.results)
                        OperationsWorkActivity.objects.filter(work_id=wa.id, users_id=request.user.id, title=str(x)).update(validation=vp, your_results=d)
                        if vp == "Correct Answer":
                            owas = OperationsWorkActivity.objects.filter(work_id=wa.id, users_id=request.user.id, validation="Correct Answer").count()
                            pro = (owas/owa_c)*100
                            AveragesWorkActivity.objects.filter(type_a_id=wa.id, student_id=request.user.id).update(average=pro)
    finish = all(x.validation != None for x in owa)
    context = {"owa":owa, "name": name, "name_work": name_work, "range": range(ran),"finish": finish, "wa": wa}
    return render(request, "rooms/exams.html", context)

def deleteStudent(request, name):
    print("hola")
    c = Classes.objects.filter(name=name)
    for fil in c:
        f = fil.codes
        f = f[10:len(f)]
    c = Classes.objects.get(name=name, teacher_id= f)
    cr = Classrooms.objects.get(classess_id=c.id, student_id=request.user.id)
    wa = WorkActivity.objects.get(classess_id=c.id)
    com = Comments.objects.filter(user_comment=request.user.id, classess_id=c.id)
    awa = AveragesWorkActivity.objects.filter(student_id=request.user.id, type_a_id=wa.id)
    owa = OperationsWorkActivity.objects.filter(users_id=request.user.id, work_id=wa.id)
    com.delete()
    awa.delete()
    owa.delete()
    cr.delete()
    messages.success(request, "Exit the Classrooms")
    return redirect("listCalassrooms")

def editWorkHome(request, name, name_work):
    c = Classes.objects.filter(name=name)
    for fil in c:
        f = fil.codes
        f = f[10:len(f)]
    c = Classes.objects.get(name=name, teacher_id= f)
    wa = WorkActivity.objects.get(name_work = name_work, classess_id=c.id)
    if request.method == 'POST':
        cn = request.POST.get('changeName')
        WorkActivity.objects.filter(name_work = name_work, classess_id=c.id).update(name_work=cn)
        messages.success(request, "Change Name")
    context = {"valid": wa,'name': name, "name_work":name_work}
    return render(request, "rooms/editWork.html", context)

def editWorkExam(request, name, name_work):
    c = Classes.objects.filter(name=name)
    for fil in c:
        f = fil.codes
        f = f[10:len(f)]
    c = Classes.objects.get(name=name, teacher_id= f)
    wa = WorkActivity.objects.get(name_work = name_work, classess_id=c.id)
    if request.method == 'POST':
        cn = request.POST.get('changeName')
        WorkActivity.objects.filter(name_work = name_work, classess_id=c.id).update(name_work=cn)
        messages.success(request, "Change Name")
        if wa.type_a_id == 1:
            return redirect(f"/listCalassrooms/{name}/listHomework/")
        if wa.type_a_id == 2:
            return redirect(f"/listCalassrooms/{name}/listExam/")
    context = {"valid": wa,'name': name, "name_work":name_work}
    return render(request, "rooms/editWork.html", context)

def deleteWork(request, name, name_work):
    c = Classes.objects.filter(name=name)
    for fil in c:
        f = fil.codes
        f = f[10:len(f)]
    c = Classes.objects.get(name=name, teacher_id= f)
    wa = WorkActivity.objects.get(name_work = name_work, classess_id=c.id)
    awa = AveragesWorkActivity.objects.filter(type_a_id=wa.id)
    owa = OperationsWorkActivity.objects.filter(work_id=wa.id)
    owa.delete()
    awa.delete()
    wa.delete()
    t = "Delete Work"
    Comments.objects.create(user_comment_id= request.user.id, text=t, classess_id=c.id)
    messages.success(request, "Delete work")
    return redirect(f"/listCalassrooms/{name}/")

    
def profileClass(request, name):
    c = Classes.objects.filter(name=name)
    for fil in c:
        f = fil.codes
        f = f[10:len(f)]
    c = Classes.objects.get(name=name, teacher_id= f)
    if request.method == 'POST':
        cn = request.POST.get('changeName')
        Classes.objects.filter(name=name, teacher_id= f).update(name=cn)
        messages.success(request, "Change Name")
        return redirect('listCalassrooms')
    context = {"class": c, 'name':name}
    return render(request, "rooms/chageClass.html", context)

def deleteClass(request, name):
    c = Classes.objects.filter(name=name)
    for fil in c:
        f = fil.codes
        f = f[10:len(f)]
    c = Classes.objects.get(name=name, teacher_id= f)
    cr = Classrooms.objects.filter(classess_id=c.id, teacher_id=request.user.id)
    wa = WorkActivity.objects.filter(classess_id=c.id)
    com = Comments.objects.filter(user_comment=request.user.id, classess_id=c.id)
    com.delete()
    for x in wa:
        awa = AveragesWorkActivity.objects.filter(type_a_id=x.id)
        owa = OperationsWorkActivity.objects.filter(work_id=x.id)
        owa.delete()
        awa.delete()
    wa.delete()
    cr.delete()
    c.delete()
    
    messages.success(request, "Delete Class")
    return redirect(f"/listCalassrooms/")
    
def deleteUser(request):
    query_set = Group.objects.filter(user = request.user)

    for g in query_set:
        a = g.name
    if a == "student":
        cr = Classrooms.objects.filter(student_id = request.user.id)
        awa = AveragesWorkActivity.objects.filter(student_id = request.user.id)
        op = OperationsPractics.objects.filter(users = request.user.id)
        owa = OperationsWorkActivity.objects.filter(users = request.user.id)
        com = Comments.objects.filter(user_comment = request.user.id)
        u = User.objects.get(username = request.user.username)
        owa.delete()
        awa.delete()
        com.delete()
        cr.delete()
        op.delete()
        u.delete()
    if a == "teacher":
        u = User.objects.get(username = request.user.username)
        c = Classes.objects.filter(teacher_id = request.user.id)
        cr = Classrooms.objects.filter(teacher_id = request.user.id)
        com = Comments.objects.filter(user_comment = request.user.id)
        op = OperationsPractics.objects.filter(users = request.user.id)
        op.delete()
        com.delete()
        for i in c:
            wa = WorkActivity.objects.get(classess_id=i.id)
            awa = AveragesWorkActivity.objects.filter(type_a_id=wa.id)
            owa = OperationsWorkActivity.objects.filter(work_id=wa.id)
            owa.delete()
            awa.delete()
            wa.delete()
        cr.delete()
        c.delete()
        u.delete()

    return redirect('index')