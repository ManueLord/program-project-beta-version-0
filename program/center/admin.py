from django.contrib import admin
from .models import *

admin.site.register(TypeClass)
admin.site.register(Classes)
admin.site.register(Classrooms)
admin.site.register(TypeActivity)
admin.site.register(WorkActivity)
admin.site.register(AveragesWorkActivity)
admin.site.register(OperationsWorkActivity)
admin.site.register(OperationsPractics)
admin.site.register(Comments)
admin.site.register(Profile)


# Register your models here.
