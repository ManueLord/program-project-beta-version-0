# Generated by Django 3.0.4 on 2020-04-20 05:03

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('center', '0003_auto_20200420_0457'),
    ]

    operations = [
        migrations.RenameField(
            model_name='comments',
            old_name='user',
            new_name='user_comment',
        ),
        migrations.AlterField(
            model_name='comments',
            name='time',
            field=models.CharField(default='Mon Apr 20 2020, 05:03:27', max_length=50),
        ),
    ]
