# Generated by Django 3.0.4 on 2020-04-22 07:14

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('center', '0008_auto_20200422_0516'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='averagesworkactivity',
            name='finish_time',
        ),
        migrations.RemoveField(
            model_name='averagesworkactivity',
            name='start_date',
        ),
        migrations.AddField(
            model_name='averagesworkactivity',
            name='active',
            field=models.BooleanField(blank=True, default=False, null=True),
        ),
        migrations.AlterField(
            model_name='comments',
            name='time',
            field=models.CharField(default='Wed Apr 22 2020, 07:14:17', max_length=50),
        ),
    ]
