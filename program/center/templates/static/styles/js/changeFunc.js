function elementExam(){
    var a = document.getElementById("myDIV");
    a.style.display = "block";
    var b = document.getElementById("myDIVE");
    b.style.display = "block";
    var c = document.getElementById("myDIVS");
    c.style.display = "none";
}

function elementHome(){
    var a = document.getElementById("myDIV");
    a.style.display = "none";
    var b = document.getElementById("myDIVE");
    b.style.display = "none";
    var c = document.getElementById("myDIVS");
    c.style.display = "block";
}

function changeFunc() {
    var selectBox = document.getElementById("selectBox");
    var selectedValue = selectBox.options[selectBox.selectedIndex].value;
    if (selectedValue === '1'){
        elementExam()
    }
    if (selectedValue === '2'){
        elementHome()
    }
}