import java.util.*;
import java.text.DecimalFormat;

public class generateRandomArithmetic {
    
    private static double getRandom(double min, double max){
        double x = (Math.random()*((max-min)+1))+min;
        return x;
    }
    
    private static String[] converter(String neg, String dec, int maxim){
        DecimalFormat df = new DecimalFormat("#######.##");
        int [] v = new int[4];
        String [] r = new String[4];
        double n1 = getRandom(0, maxim);
        double n2 = getRandom(0, maxim);
        for (int i = 0; i < 4; i++){
            int random = new Random().nextInt(10);
            v[i] = (int) random;
        }
        if(dec.equals("d")){
            if(v[0] % 2 == 0){
                int n = (int)n1;
                r[0] = String.valueOf(n);
            }else{
                r[0] = df.format(n1);
            }
            if(v[1] % 2 == 0){
                int n = (int)n2;
                r[1] = String.valueOf(n);
            }else{
                r[1] = df.format(n2);
            }
        }else{
            int nu1 = (int)n1;
            r[0] = String.valueOf(nu1);
            int nu2 = (int)n2;
            r[1] = String.valueOf(nu2);
            
        }
        if(neg.equals("n")){
            if(v[2] % 2 == 0){
                r[0] = "-"+r[0];
                r[2] = "($"+ r[0]+"$)";
            }else{
                r[2] = r[0];
            }
            if(v[3] % 2 == 0){
                r[1] = "-"+r[1];
                r[3] = "($"+ r[1] +"$)";
            }else{
                r[3] = r[1];
            }
        }
        return r;
    }
    
    private static double getResultDouble(double n1, double n2, String ope){
        double x = 0;
        if (ope.equals("+")){
            x = n1 + n2;
        }
        if (ope.equals("-")){
            x = n1 - n2;
        }
        if (ope.equals("z")){
            x = n1 * n2;
        }
        if (ope.equals("/")){
            x = n1 / n2;
        }
        return x;
    }
    
    private static int getResultInteger(int n1, int n2, String ope){
        int x = 0;
        if (ope.equals("+")){
            x = n1 + n2;
        }
        if (ope.equals("-")){
            x = n1 - n2;
        }
        if (ope.equals("z")){
            x = n1 * n2;
        }
        return x;
    }
    
    public static void main(String args[]) {
        DecimalFormat df = new DecimalFormat("#######.##");
        if (args.length == 0) {
            System.out.println("year is required");
        }else{
        
        int maxim = Integer.parseInt(args[0]);
        String neg = args[1], dec = args[2], ope = args[3], res3 = "";
        String res[] = converter(neg, dec, maxim);
        
        if(dec.equals("d")){
            double r1, r2, r3;
            r1 = Double.valueOf(res[0]);
            r2 = Double.valjava -cp codes/generators/arithmetic/code generateRandomArithmeticueOf(res[1]);
            r3 = getResultDouble(r1, r2, ope);
            res3 = String.valueOf(df.format(r3));
        }else{
            int r1, r2, r3;
            r1 = Integer.parseInt(res[0]);
            r2 = Integer.parseInt(res[1]);
            if(ope.equals("/")){
                double re = getResultDouble(Double.valueOf(r1), Double.valueOf(r2), ope);
                res3 = String.valueOf(df.format(re));
            }else{
                r3 = getResultInteger(r1, r2, ope);
                res3 = String.valueOf(df.format(r3));
            }
        }
        if(ope.equals("/")){
            ope = "\\div";
        }
        if(ope.equals("z")){
            ope = "*";
        }
        if(neg.equals("n")){
            System.out.print(res[2] + " " + ope + " " + res[3] + " = " + res3);
        }else{
            System.out.print(res[0] + " " + ope + " " + res[1] + " = " + res3);
        }
        }
    }
}
